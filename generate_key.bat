ECHO OFF
REM  QBFC Project Options Begin
REM  HasVersionInfo: Yes
REM  Companyname: 
REM  Productname: simple ssh-keygen for windows environment
REM  Filedescription: This is simple ssh-keygen with key generation instruction for windows
REM  Copyrights: 
REM  Trademarks: 
REM  Originalname: simple-ssh-keygen.exe
REM  Comments: Create by Chinnawat Wipawapat (chinkung_devils@hotmail.com)
REM  Productversion: 01.00.00.00
REM  Fileversion: 01.00.00.00
REM  Internalname: 
REM  Appicon: secrecy.ico
REM  Embeddedfile: generate_key.bat
REM  Embeddedfile: msys-1.0.dll
REM  Embeddedfile: msys-crypto-0.9.8.dll
REM  Embeddedfile: msys-minires.dll
REM  Embeddedfile: ssh-keygen.exe
REM  QBFC Project Options End
ECHO ON
ECHO OFF
ECHO OFF
@echo off
cls
echo.
echo This is standalone simple ssh-keygen for Windows
echo By Chinnawat Wipawapat (chinkung_devils@hotmail.com)
echo.
echo Please follow the instruction to generate new SSH key
:asktype
echo.
set keytype=""
echo Please choose key type:
echo 1. RSA (Recommended)
echo 2. DSA
set /p keytype=Choose: 
if %keytype% == 1 goto rsa
if %keytype% == 2 goto dsa
goto asktype

:dsa
set keytype=dsa
echo Key length will be automatically set to 1024 bits for DSA
goto set1024

:rsa
echo.
set keytype=rsa
set keylength=""
echo Please choose key length (bits):
echo 1. 1024
echo 2. 2048 (Recommended)
echo 3. 4096
set /p keylength=Choose: 
if %keylength% == 1 goto set1024
if %keylength% == 2 goto set2048 
if %keylength% == 3 goto set4096
goto rsa

:set1024
set keylength=1024
goto askcomment

:set2048
set keylength=2048
goto askcomment

:set4096
set keylength=4096
goto askcomment

:askcomment
echo.
set keycomment=""
set /p keycomment=Please enter key comment: 
if %keycomment% == "" goto askcomment

:askname
echo.
set keyname=""
set /p keyname=Please enter output key name: 
if %keyname% == "" goto askname
echo.
%MYFILES%\ssh-keygen.exe -b %keylength% -t %keytype% -C %keycomment% -f %keyname%
echo.
pause
